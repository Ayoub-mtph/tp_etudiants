<?php
  require_once '../src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {
   Private $Poney;
   public function setUp(){
    $Poney=new Poneys();
    $Poney->setCount(10);
   } 
  public function tearDown(){
    unset($this->Poney);
   }
/**
  * @dataProvider numberProvider
  */

   public function test_removePoneyFromField($a) {
      $Poney= new Poneys();

      // Action
      $Poneys->removePoneyFromField($a);
      
      // Assert
      $this->assertTrue($Poneys->getCount()>=0);
    }
   public function numberProvider(){
     return array(
       array('5'),
       array('6'),
       array('10'),
       );


    }
   /* public function test_getNames(){
      $mock=$this->getMockBuilder(Poneys::class)->setMethods(['getNames']->getMock();
      $mock->expects(exactly(1))->method('getNames')->willReturn(['Poney','Pooney']);
      $this->assertEquals(['Poney','Pooney'],$mock->getNames());
    }*/
   /* public function setUp(){
     $Poney = new Poneys();
     $Poney.
     $Poney->removePoneyFromField(10);
     $this->assertTrue($Poney->checkPoney());
    }*/
    
  }
 ?>
